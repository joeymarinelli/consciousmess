Rails.application.routes.draw do
  get 'ripples/next' => 'ripples#next_ripple'
  get 'ripples/previous' => 'ripples#previous_ripple'
  get 'ripples/' => 'ripples#newest_ripples'
  get 'ripples/oldest' => 'ripples#oldest_ripples'
  resources :ripples
  root 'ripples#index', as: 'ripples_index' 

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
