module RipplesHelper
  def contains_url ripple
    if ripple.url.present?
      if /^[(http)(https)]/.match(ripple.url[0..4])
	link_to(ripple.name, ripple.url)
      else
          link_to(ripple.name, "http://#{ripple.url}")
      end
    else
      ripple.name
    end
  end

  def get_min_id ripples
    ripples.minimum(:id)  
  end
end

