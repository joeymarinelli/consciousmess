class RipplesController < ApplicationController
  before_action :set_ripple, only: [:show, :edit, :update, :destroy]
  before_action :redirect_user, only: [:edit, :update, :destroy]
  before_action :set_page, only: [:index]

  # GET /ripples
  # GET /ripples.json
  def index
      @min_id = Ripple.minimum(:id)
      @ripples = Ripple.all.order(created_at: :desc).limit(10).offset(session[:page] * 10)
  end

  # GET /ripples/1
  # GET /ripples/1.json
  def show
  end

  # GET /ripples/new
  def new
    @ripple = Ripple.new
  end

  # GET /ripples/1/edit
  def edit
  end

  # POST /ripples
  # POST /ripples.json
  def create
    @ripple = Ripple.new(ripple_params)
    respond_to do |format|
      if @ripple.save
        format.html { redirect_to @ripple, notice: 'Ripple was successfully created.' }
        format.json { render :show, status: :created, location: @ripple }
      else
        format.html { render :new }
        format.json { render json: @ripple.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ripples/1
  # PATCH/PUT /ripples/1.json
  def update
    respond_to do |format|
      if @ripple.update(ripple_params)
        format.html { redirect_to @ripple, notice: 'Ripple was successfully updated.' }
        format.json { render :show, status: :ok, location: @ripple }
      else
        format.html { render :edit }
        format.json { render json: @ripple.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ripples/1
  # DELETE /ripples/1.json
  def destroy
    @ripple.destroy
    respond_to do |format|
      format.html { redirect_to ripples_url, notice: 'Ripple was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def next_ripple
      session[:page] += 1
      redirect_to '/'
  end

  def previous_ripple
    session[:page] -= 1
    redirect_to '/'
  end

  def newest_ripples
    session[:page] = 0
    redirect_to '/'
  end

  def oldest_ripples
    if Ripple.count % 10 != 0
      session[:page] = Ripple.count/10.to_f.ceil
    else
      session[:page] = Ripple.count/10.to_f.ceil - 1
    end
    redirect_to '/'
  end
  private
    def set_page
      if !session[:page]
        session[:page] = 0
      end
      @page = session[:page]
    end
    def redirect_user
      redirect_to @ripple
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_ripple
      @ripple = Ripple.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ripple_params
      params.require(:ripple).permit(:name, :url, :message)
    end

end
