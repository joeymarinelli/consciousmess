require 'test_helper'

class RipplesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ripple = ripples(:one)
  end

  test "should get index" do
    get ripples_url
    assert_redirected_to '/'
  end

  test "should get new" do
    get new_ripple_url
    assert_response :success
  end

  test "should create ripple" do
    assert_difference('Ripple.count') do
      post ripples_url, params: { ripple: { message: @ripple.message, name: @ripple.name, url: @ripple.url } }
    end

    assert_redirected_to ripple_url(Ripple.last)
  end

  test "should show ripple" do
    get ripple_url(@ripple)
    assert_response :success
  end

  test "should get edit" do
    get edit_ripple_url(@ripple)
    assert_redirected_to(@ripple)
  end

  test "should update ripple" do
    patch ripple_url(@ripple), params: { ripple: { message: @ripple.message, name: @ripple.name, url: @ripple.url } }
    assert_redirected_to ripple_url(@ripple)
  end

  test "should increase page" do 
     get ripples_url
     get ripples_next_url
     assert_equal 1, session[:page]
     assert_redirected_to '/'
  end

  test "should decrease page" do 
    get ripples_url
    get ripples_next_url
    get ripples_previous_url
    assert_equal 0, session[:page]
    assert_redirected_to '/'
  end

  test "should get newest page" do 
    get ripples_url
    get ripples_oldest_url
    get ripples_url
    assert_equal 0, session[:page]
    assert_redirected_to '/'
  end

  test "should get oldest page" do 
    get ripples_url
    get ripples_oldest_url
    assert_equal 5, session[:page]
    assert_redirected_to '/'
  end
end
