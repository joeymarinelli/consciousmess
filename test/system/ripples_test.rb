require "application_system_test_case"

class RipplesTest < ApplicationSystemTestCase
  setup do
    @ripple = ripples(:one)
  end

  test "visiting the index" do
    visit ripples_url
    assert_selector "h1", text: "CONSCIOUS/mess"
  end

  test "creating a Ripple" do
    visit ripples_url
    click_on "New Ripple"
    fill_in "message", with: "My Message"
    fill_in "name", with: "My Name"
    fill_in "url", with: "myurl.com"
    click_on "Create Ripple"
    
    assert_text "Ripple was successfully created"
    click_on "Back"
  end

  test "navigate to oldest" do
    visit ripples_url
    click_on "Oldest"
    #assert_redirected_to '/ripples/oldest'
    assert_selector "i", text: "Next 10 Ripples"
  end

  test "navigate to newest" do
    visit ripples_url
    click_on "Oldest"
    click_on "Newest"
    #assert_redirected_to '/ripples/oldest'
    assert_selector "i", text: "Previous 10 Ripples"
  end

  test "navigate to next ten" do
    visit ripples_url
    click_on "Newest"
    click_on "Next 10 Ripples"
    assert_equal 11, page.all('tr').count
  end

  test "navigate to previous ten" do
    visit ripples_url
    click_on "Oldest"
    click_on "Previous 10 Ripples"
    assert_equal 11, page.all('tr').count
  end


end
