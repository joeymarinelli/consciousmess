class ChangeMesageToMessage < ActiveRecord::Migration[5.2]
  def change
    rename_column :ripples, :mesage, :message
  end
end
